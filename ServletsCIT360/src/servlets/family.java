package servlets;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

@WebServlet(name = "/family", urlPatterns = {"/family"})
public class family extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        ArrayList<Familylist> familyList = new ArrayList<Familylist>();
        familyList.add(0, new Familylist(1,"Trevon",38));
        familyList.add(1, new Familylist(2,"Charmayne",40));
        familyList.add(2, new Familylist(3,"Trevaye",20));
        familyList.add(3, new Familylist(4,"Charvon",14));

        Familylist trevon = familyList.get(0);
        Familylist charmayne = familyList.get(1);
        Familylist trevaye = familyList.get(2);
        Familylist charvon = familyList.get(3);
        // Set response content type
        response.setContentType("text/html");

        PrintWriter out = response.getWriter();
        String title = "Reading Family Values";
        String docType =
                "<!doctype html public \"-//w3c//dtd html 4.0 " + "transitional//en\">\n";

        out.println(docType +
                        "<html>\n" +
                        "<head><title>" + title + "</title></head>\n" +
                        "<body bgcolor = \"#f0f0f0\">\n" +
                        "<h1 align = \"center\">" + title + "</h1>\n" +
                        "<table width = \"100%\" border = \"1\" align = \"center\">\n" +
                        "<tr bgcolor = \"#949494\">\n" +
                        "<th>Family Name</th>"+
                "<th> Age</th>\n"+
                        "</tr>\n");

        out.print("<tr><td>" + trevon.name + "</td>"+"<td>" + trevon.age +"\n");
        out.print("<tr><td>" + charmayne.name + "</td>"+"<td>" + charmayne.age +"\n");
        out.print("<tr><td>" + trevaye.name + "</td>"+"<td>" + trevaye.age +"\n");
        out.print("<tr><td>" + charvon.name + "</td>"+"<td>" + charvon.age +"\n");

        out.println("</tr>\n</table>\n</body></html>");

    }
}
