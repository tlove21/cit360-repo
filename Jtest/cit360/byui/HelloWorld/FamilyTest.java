package cit360.byui.HelloWorld;

import com.sun.istack.internal.NotNull;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Array;
import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

class FamilyTest {

    //create a family member as I did in the JOBJ class last week
    Family fam= new Family(2,"Trevon",40);
    //create another one to simulate an issue I had with multiple Array
    Family fam2= new Family(3,"Charms",40);
    //Create a null member as I did last week to check if it asserts as Null
    Family fam3= null;

    //Create 2 array lists as I did last week, to simulate
    ArrayList<Family> familyList = new ArrayList<Family>();
    ArrayList<Family> familyList2 = new ArrayList<Family>();


    private Object NotNull;

    @Test
    void getOrderno() {
        //Assert Equals check if int is same as the int returned
        assertEquals(2,fam.getOrderno());
        System.out.println("Assert Equals Success");

        //Assert Not Null checks if not empty
        assertNotNull(fam.getOrderno());
        System.out.println("Assert Not NUll Success");
    }


    @Test
    void getName() {
        // Asserts not same checks if two objects are not equal
        assertNotSame(fam2.getOrderno(),fam.getName());
        System.out.println("Assert Not Same Success");

    }



    @Test
    void getAge() {
        //Asset same can check two objects and
        assertSame(fam.getAge(),fam2.getAge());

        System.out.println("Assert Same Success");


    }

    @Test
    void trueOrFalse(){
        //I could check if age is null or if anyone in the family has the same age
        boolean fibber= true;
        //Check if two members are of the same age.
        if (fam.getAge() == fam2.getAge()){
            assertTrue(fibber);
            System.out.println("Assert True Success");
        }else{
            fibber=false;
            assertFalse(fibber);
        }

        //If I change fibber to false this if statement runs
        if (fam.getAge() != fam2.getAge()){
            assertFalse(fibber);
            System.out.println("Assert False Success");
        }else{
            fibber=true;
            assertTrue(fibber);
        }

    }

    @Test
    void arrayTester(){
        //Reuse all the family member above and add two new ones
        familyList.add(0, new Family(1,"Trevaye",20));
        familyList.add(1, new Family(2,"Charvon",20));
        familyList.add(fam);
        familyList.add(fam2);

        //If I saved the Array to a file would I get a 1:1 to replica when I reverse the process.
        familyList2 = familyList;

        //A simple check to see if the Arrays are equal show that I can replicate an Array anywhere
        assertArrayEquals(familyList.toArray(),familyList2.toArray());
        System.out.println("Assert Arrays Equals Success");
    }

    @Test
    void nullTester(){
        //Just check if an Array is truly null, and can be used when you have tonnes of code and need to do a check
        assertNull(fam3);
        System.out.println("Assert Null Success");
    }

}