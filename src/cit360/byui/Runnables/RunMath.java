package cit360.byui.Runnables;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class RunMath {

    public static void main(String[] args){

        ExecutorService myMathRunner = Executors.newFixedThreadPool(4);

        DoMath dm1 = new DoMath(25, 5, 1000);
        DoMath dm2 = new DoMath(5, 25, 500);
        DoMath dm3 = new DoMath(25, 25, 2000);
        DoMath dm4 = new DoMath(5, 5, 50);
        DoMath dm5 = new DoMath(25, 50, 100);
        DoMath dm6 = new DoMath(50, 50, 50);

        myMathRunner.execute(dm1);
        myMathRunner.execute(dm2);
        myMathRunner.execute(dm3);
        myMathRunner.execute(dm4);
        myMathRunner.execute(dm5);
        myMathRunner.execute(dm6);

        myMathRunner.shutdown();

    }

}
