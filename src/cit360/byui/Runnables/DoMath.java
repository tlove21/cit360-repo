package cit360.byui.Runnables;

public class DoMath implements Runnable{

    private int number1;
    private int number2;
    private int sleep;
    private int answer;

    public DoMath(int number1, int number2, int sleep){

        this.number1 = number1;
        this.number2 = number2;
        this.sleep = sleep;



    }
    @Override
    public void run() {

        int i;
        if (this.number1 > this.number2){
            this.answer = this.number1 / this.number2;
            for(i=0;i<100;i++){
                System.out.println("\n\nDue to " + number1 + " being bigger than " + number2 + " We Divide and get: " + answer + " With sleep: " + sleep);
            }
        }else if(this.number1 == this.number2){
            this.answer = this.number1 + this.number2;
            for(i=0;i<100;i++) {
                System.out.println("\n\nDue to " + number1 + " being equal to " + number2 + " We Add and get: " + answer + " With sleep: " + sleep);
            }
        }else{
            this.answer = this.number1 * this.number2;
            for(i=0;i<100;i++){
            System.out.println("\n\nDue to " + number1 + " being smaller than " + number2 + " We Multiply and get: " + answer + " With sleep: " + sleep);
        }}


        try {
            Thread.sleep(sleep);
        } catch (InterruptedException e) {
            System.err.println(e.toString());
        }


    }
}
