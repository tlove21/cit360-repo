package cit360.byui.hibernate;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import java.util.*;

public class testDAO {

    SessionFactory factory = null;
    Session session = null;

    private static testDAO single_instance = null;

    private testDAO()
    {
        factory = HibernateUtilities.getSessionFactory();
    }

    /** This is what makes this class a singleton.  You use this
     *  class to get an instance of the class.
     * @return*/
    public static testDAO getInstance()
    {
        if (single_instance == null) {
            single_instance = new testDAO();
        }

        return single_instance;
    }

    /** Used to get more than one customer from database
     *  Uses the OpenSession construct rather than the
     *  getCurrentSession method so that I control the
     *  session.  Need to close the session myself in finally.*/
    public List<Users> getUser() {

        try {
            session = factory.openSession();
            session.getTransaction().begin();
            String sql = "from cit360.byui.hibernate.Users";
            List<Users> us = (List<Users>)session.createQuery(sql).getResultList();
            session.getTransaction().commit();
            return us;

        } catch (Exception e) {
            e.printStackTrace();
            // Rollback in case of an error occurred.
            session.getTransaction().rollback();
            return null;
        } finally {
            session.close();
        }

    }

    /** Used to get a single customer from database
     * @return*/
    public Users getUsers(int id) {

        try {
            session = factory.openSession();
            session.getTransaction().begin();
            String sql = "from cit360.byui.hibernate.Users where id=" + Integer.toString(id);
            Users u = (Users)session.createQuery(sql).getSingleResult();
            session.getTransaction().commit();
            return u;

        } catch (Exception e) {
            e.printStackTrace();
            // Rollback in case of an error occurred.
            session.getTransaction().rollback();
            return null;
        } finally {
            session.close();
        }
    }

}
