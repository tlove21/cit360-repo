package cit360.byui.TryCatch;

public class CustomExceptions extends Exception{

    public CustomExceptions() {
    }

    public CustomExceptions(String string) {
        super(string);
    }

    public CustomExceptions(String string, Throwable thrwbl) {
        super(string, thrwbl);
    }

    public CustomExceptions(Throwable thrwbl) {
        super(thrwbl);
    }

    public CustomExceptions(String string, Throwable thrwbl, boolean bln, boolean bln1) {
        super(string, thrwbl, bln, bln1);
    }
}
