package cit360.byui.TryCatch;


import java.util.Scanner;


public class ExceptionCatch {

    public static void main(String[] args){

        //Create mini price checker to calculate price of an item
        Scanner selector = new Scanner(System.in);  // Create a Scanner object
        String accessVar;
        int num=0;
        boolean validator;

        //Create an object to call the function
        ShopManager s=new ShopManager();

        //Display Simple Interface
        System.out.println("\n");
        System.out.println("|'''' Shopping Cart '''''''|");
        System.out.println("| List Selection:          |");
        System.out.println("| Enter 0 Divide 2 numbers |");
        System.out.println("| Enter 1 Calculate Item   |");
        System.out.println("| Enter 2 View Full Cost   |");
        System.out.println("| Enter 3 to Exit          |");
        System.out.println("|..........................|");
        accessVar= selector.nextLine();  // Accept Number from User

        validator=s.isNumeric(accessVar);

        if(validator==false) {
            main(new String[]{""});
        }else{
            num = Integer.parseInt(accessVar);
        }

        s.shopCheck(num);
    }

}


