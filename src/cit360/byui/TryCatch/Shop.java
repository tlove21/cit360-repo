package cit360.byui.TryCatch;

import java.io.Serializable;

public class Shop implements Serializable {

    public int itemNo;
    public String itemName;
    public double itemPrice;
    public int itemQuant;

    //Constructor to aid with deserialization
    public Shop(int itemNo,String itemName,double itemPrice, int itemQuant){
        this.itemNo=itemNo;
        this.itemName=itemName;
        this.itemPrice=itemPrice;
        this.itemQuant=itemQuant;
    }

    public int getitemno() {
        return itemNo;
    }
    public void setItemNo(int order) {
        this.itemNo = itemNo;
    }

    public String getName() {
        return itemName;
    }
    public void setName(String itemName) { this.itemName = itemName; }

    public double getItemPrice() {
        return itemPrice;
    }
    public void setItemPrice(double itemPrice) {
        this.itemPrice = itemPrice;
    }

    public int getItemQuant() {
        return itemQuant;
    }
    public void setItemQuant(int itemQuant) {
        this.itemQuant = itemQuant;
    }

}
