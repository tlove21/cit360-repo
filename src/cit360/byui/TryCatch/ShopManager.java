package cit360.byui.TryCatch;

import jdk.internal.org.objectweb.asm.tree.TryCatchBlockNode;

import java.io.*;
import java.util.Scanner;

import static cit360.byui.TryCatch.ExceptionCatch.main;

public class ShopManager {


     static String filename = "output.txt";

     public static boolean isNumeric(String strNum) {
          if (strNum == null) {
               return false;
          }
          try {
               int d = Integer.parseInt(strNum);
          } catch (NumberFormatException err) {
               System.out.println("Please enter an Integer");
               return false;
          }
          return true;
     }

     public void shopCheck(int x){

          //Use Switch Statement to Call Functions, Exit, or warn of wrong input
          switch (x) {
               case 0:
                    //Call the Divider Function
                    divNums();
                    break;
               case 1:
                    //Call Add, Delete Function
                    AddObj();
                    break;
               case 2:
                    //Call the Store Viewer
                    ViewObj();
                    break;
               case 3:
                    System.out.println("Exiting Shop Manager");
                    System.exit(0);
               default:
                    //restart the program
                    System.out.println("That Option Is Not Accepted");
                    main(new String[]{""});
                    break;
          }

     }

     private void divNums() {


          int num1 = (int) getIntInput("Input Num1: ");
          int num2 = (int) getIntInput("Input Num2: ");

               try
               {
                    int divided = num1 / num2;
                    int rem = num1 % num2;
                    System.out.println("Your divided Answer is: " + divided + " Remainder: "+rem);
                    main(new String[]{""});
               } catch (ArithmeticException divzero) {
                    System.out.println("Detected you tried to divide by zero, try again");
                    divNums();

               }



     }

     private void AddObj() {

          String tempName="";
          double tempCost=0;

          Scanner sc = new Scanner(System.in);


          int tempNo = (int) getIntInput("Enter Item No\n");
          System.out.println("Enter Item Name");
          tempName = sc.nextLine();
          tempCost = getDoubleInput("Enter Item Cost\n");
          int tempQuant = (int) getIntInput("Enter Item Quantity\n");


          Shop items= new Shop(tempNo,tempName,tempCost,tempQuant);



          // Serialization begins hoorah
          try
          {
               //Saving of object in a file
               FileOutputStream file = new FileOutputStream(filename);
               ObjectOutputStream out = new ObjectOutputStream(file);

               // Method for serialization of object
               out.writeObject(items);

               //Close the stream
               out.close();
               file.close();

               //Confirm save of file
               System.out.println("Item saved for viewing later and will display now");
               //Call the display
               ViewObj();

          }
          //Input Output Exception
          catch(IOException ex)
          {
               System.out.println("IOException file cannot be saved");
          }

          System.out.println("Add Delete Called");
          main(new String[]{""});

     }

     private static void ViewObj() {

          Shop itemload = null;
          try
          {
               // Read file for the object
               FileInputStream file = new FileInputStream(filename);
               ObjectInputStream read = new ObjectInputStream(file);

               // Deserialization of object
               itemload = (Shop)read.readObject();

               //Close the Streams
               read.close();
               file.close();

               //Calculate the price from the deserialized double and int
               double price = itemload.itemPrice * itemload.itemQuant;

               //Output the item and final price of item
               System.out.println("Shop: Item No:" + itemload.itemNo +" | Item Name:"+ itemload.itemName +  " | Item Price:"+itemload.itemPrice + " | Item Quantity:"+itemload.itemQuant + "\nFinal Price: "+Math.round(price*100.0)/ 100.0);
               main(new String[]{""});
          }
          //Input Output Exception Catcher
          catch(IOException ex)
          {
               System.out.println("File does not exist, create one by using the first option");
               main(new String[]{""});
          }
          //In case for some reason the text file does not return the Class
          catch(ClassNotFoundException ex)
          {
               System.out.println("Object Class cannot be found, try creating a file first with option 1");
               main(new String[]{""});
          }


          main(new String[]{""});
     }

     //A reusable try catch integrated fynction that wont quit and ensures data is valid
     double getIntInput(String message) {
          Scanner sc = new Scanner(System.in);
          try {
               System.out.print(message);
               return sc.nextInt();
          }catch(Exception e) {
               System.out.print("Please enter an integer, no decimal, or String accepted, please try again\n");
               return getIntInput(message);
          }
     }
     //A reusable try catch integrated fynction that wont quit and ensures data is valid
     double getDoubleInput(String message) {
          Scanner d1 = new Scanner(System.in);
          try {
               System.out.print(message);
               return d1.nextDouble();
          }catch(Exception e) {
               System.out.print("This input does not accept Strings, input can be an integer or decimal, please try again\n");
               return getDoubleInput(message);
          }
     }

}
