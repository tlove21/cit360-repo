package cit360.byui.JSONHTTPRequests;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;

import java.net.URL;
import java.nio.file.Paths;
import java.util.List;

import cit360.byui.HelloWorld.Family;
import com.fasterxml.jackson.core.*;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.*;
import sun.net.www.http.HttpClient;

import javax.net.ssl.HttpsURLConnection;

public class HTTPString {

    public static HttpURLConnection connection;

    static void urlConnection() {

        BufferedReader readin;
        String resString;
        StringBuffer responseString = new StringBuffer();
        HTTPServerDemo hs = new HTTPServerDemo();
        hs.serverMain();

        try {
            URL url = new URL("http://localhost:8080/");
            try {
                connection = (HttpURLConnection) url.openConnection();
            }catch (Exception h){
                System.out.println("Nothing is being read "+ h);
            }
            //Request protocols
            connection.setRequestMethod("GET");
            connection.setConnectTimeout(5000);
            connection.setReadTimeout(6000);

            //Server Responses
            int response = connection.getResponseCode();
            //System.out.println("Response Content: "+response);

            //Endpoint response
            if (response > 299) {
                readin = new BufferedReader(new InputStreamReader(connection.getErrorStream()));
                while ((resString = readin.readLine()) != null) {
                    responseString.append(resString);
                }
                readin.close();
            } else {
                readin = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                while ((resString = readin.readLine()) != null) {
                    responseString.append(resString);
                }
                readin.close();

            }
            System.out.println("Response Content: "+responseString.toString());
            convPayLoadOBJ(responseString.toString());
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            connection.disconnect();
        }
    }

        //Convert Payload to Object
    public static void convPayLoadOBJ(String payload){



        ObjectMapper mapJSON = new ObjectMapper();
        List<Family> jsonFamin = null;

        try {
            jsonFamin = new ObjectMapper().readValue(payload,
                    new TypeReference<List<Family>>() {
                    });
        } catch (JsonParseException jsonParseException) {
            System.out.println("Parse error, restarting");
            JSONCalls.main(new String[]{""});
        } catch (JsonMappingException jsonMappingException) {
            System.out.println("Mapping error");
            JSONCalls.main(new String[]{""});
        } catch (IOException ioException) {
            System.out.println("You have not yet saved any files");
            JSONCalls.main(new String[]{""});
        }

        for (Family family : jsonFamin) {
            System.out.println("Order No: " + family.getOrderno()
                    +" Member Name: " + family.getName()
                    +" Member Age: " + family.getAge());
        }
        JSONCalls.main(new String[]{""});


    }






}
