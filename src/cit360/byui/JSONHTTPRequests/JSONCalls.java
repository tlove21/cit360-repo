package cit360.byui.JSONHTTPRequests;

import java.util.Scanner;

import static cit360.byui.TryCatch.ExceptionCatch.main;

public class JSONCalls {
    public static void main(String[] args){

        //Create mini price checker to calculate price of an item
        Scanner selector = new Scanner(System.in);  // Create a Scanner object
        String accessVar;
        int num=0;
        boolean validator;

        //Display Simple Interface
        System.out.println("\n");
        System.out.println("|'''' JSON HTTP Examples ''|");
        System.out.println("| List Selection:          |");
        System.out.println("| Enter 1 OBJ to JSON Test |");
        System.out.println("| Enter 2 JSON to OBJ Test |");
        System.out.println("| Enter 3 HTTP Server Run  |");
        System.out.println("| Enter 4 HTTP Server Call |");
        System.out.println("| Enter 5 to Exit          |");
        System.out.println("|..........................|");
        accessVar= selector.nextLine();  // Accept Number from User

        validator=isNumeric(accessVar);

        if(validator==false) {
            main(new String[]{""});
        }else{
            num = Integer.parseInt(accessVar);
        }
        shopCheck(num);
    }

    public static boolean isNumeric(String strNum) {
        if (strNum == null) {
            return false;
        }
        try {
            int d = Integer.parseInt(strNum);
        } catch (NumberFormatException err) {
            System.out.println("Please enter an Integer");
            return false;
        }
        return true;
    }

    public static void shopCheck(int x){

        JSONOBJ j = new JSONOBJ();
        HTTPString h = new HTTPString();
        HTTPServerDemo s = new HTTPServerDemo();
        //Use Switch Statement to Call Functions, Exit, or warn of wrong input
        switch (x) {
            case 1:
                //Convert Obj to JSON and write file
                j.convOBjJSON();
                break;
            case 2:
                //Call the JSON file and convert to Obj
                j.convJSONOBJ();
                break;
            case 3:
                //Do nothing
                s.serverMain();
                break;
            case 4:
                //Do nothing
                h.urlConnection();
                break;
            case 5:
                System.out.println("Exiting JSON and HTTPServer Demo");
                System.exit(0);
            default:
                //restart the program
                System.out.println("That Option Is Not Accepted");
                main(new String[]{""});
                break;
        }

    }


}
