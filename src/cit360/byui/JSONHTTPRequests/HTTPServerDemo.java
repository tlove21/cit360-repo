package cit360.byui.JSONHTTPRequests;


import cit360.byui.HelloWorld.Family;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sun.net.httpserver.HttpContext;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;

import sun.net.httpserver.HttpServerImpl;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.nio.file.Paths;
import java.util.List;

public class HTTPServerDemo{

    public static void serverMain(){

        //Check for file and load up web content
        String content="";
        ObjectMapper mapJSON = new ObjectMapper();
        File jsonfile = new File("family.json");
        List<Family> jsonFamin = null;
        try {
            jsonFamin = mapJSON.readValue(Paths.get(String.valueOf(jsonfile)).toFile(),
                    new TypeReference<List<Family>>() {
                    });
            content =mapJSON.writeValueAsString(jsonFamin);
            System.out.println("File loaded into Content");
        } catch (JsonParseException jsonParseException) {
            System.out.println("Parse error, restarting");
            JSONCalls.main(new String[]{""});
        } catch (JsonMappingException jsonMappingException) {
            System.out.println("Mapping error");
            JSONCalls.main(new String[]{""});
        } catch (IOException ioException) {
            System.out.println("No file found, Default content loaded");
            content = "[{\"name\":\"Trevon\",\"age\":40,\"orderno\":1},{\"name\":\"Charm\",\"age\":42,\"orderno\":2}]";
        }


        try {
            HttpServer httpServer = HttpServerImpl.create(new InetSocketAddress(8080),0);
            HttpContext cont = httpServer.createContext("/");
            String finalContent = content;
            cont.setHandler(new HttpHandler() {
                @Override
                public void handle(HttpExchange httpExchange) throws IOException {

                    httpExchange.sendResponseHeaders(200, finalContent.length());
                    OutputStream out = httpExchange.getResponseBody();
                    out.write(finalContent.getBytes());
                    out.close();
                }
            });
            httpServer.start();
        } catch (IOException e) {
            e.printStackTrace();
        }


    }
}
