package cit360.byui.JSONHTTPRequests;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import cit360.byui.HelloWorld.Family;

import java.io.*;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import static cit360.byui.TryCatch.ExceptionCatch.main;


public class JSONOBJ {

    public void convOBjJSON() {
        String filename = "family.json";
        int ord=0;
        String name="";
        int age=0;
        String jsonFam="";
        Scanner inpfam = new Scanner(System.in);
        ObjectMapper fammap = new ObjectMapper();

        int rotator = getIntInput("How many Family names do you want to enter?");

        ArrayList<Family> familyList = new ArrayList<Family>();
        for(int i=0; i<rotator; i++){
            ord=getIntInput("Enter Order No of family: ");
            System.out.print("Enter Name of member: ");
            name = inpfam.nextLine();
            age=getIntInput("Enter Age of member: ");
            try {
                familyList.add(i, new Family(ord, name, age));
            }catch (NullPointerException ne){
                System.out.println("Null Exception " + ne);
            }
        }

        try
        {
            //write familyList to jsonFam
            jsonFam =  fammap.writeValueAsString(familyList);
        }
        //Input Output Exception
        catch(IOException ex)
        {
            System.out.println("IOException file cannot be saved");
        }

        System.out.println("JSON String: "+jsonFam+"\n");

        try (FileWriter file = new FileWriter("family.json")) {

            file.write(jsonFam);
            file.flush();

            //Confirm save of file
            System.out.println("Item saved for viewing later and will display now\n");
            //Call the display
            convJSONOBJ();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void convJSONOBJ() {
        ObjectMapper mapJSON = new ObjectMapper();
        File jsonfile = new File("family.json");
        List<Family> jsonFamin = null;
        try {
            jsonFamin = mapJSON.readValue(Paths.get(String.valueOf(jsonfile)).toFile(),
                    new TypeReference<List<Family>>() {
                    });
            } catch (JsonParseException jsonParseException) {
            System.out.println("Parse error, restarting");
            JSONCalls.main(new String[]{""});
        } catch (JsonMappingException jsonMappingException) {
            System.out.println("Mapping error");
            JSONCalls.main(new String[]{""});
        } catch (IOException ioException) {
            System.out.println("You have not yet saved any files");
            JSONCalls.main(new String[]{""});
        }
        System.out.println();
        for (Family family : jsonFamin) {
            System.out.println("Order No: " + family.getOrderno()
                              +" Member Name: " + family.getName()
                              +" Member Age: " + family.getAge());
        }
        JSONCalls.main(new String[]{""});

    }

    int getIntInput(String message) {
        Scanner sc = new Scanner(System.in);
        try {
            System.out.print(message);
            return sc.nextInt();
        }catch(Exception e) {
            System.out.print("Please enter an integer, no decimal, or String accepted, please try again\n");
            return getIntInput(message);
        }
    }
}
