package cit360.byui.HelloWorld;

import java.util.*;

import static cit360.byui.HelloWorld.Reusables.*;

public class HelloWorld {

    public static void main(String[] args){

        Scanner selector = new Scanner(System.in);  // Create a Scanner object
        String accessVar;
        int n=0;

            System.out.println("\n");
            System.out.println("|'''''''''''''''''''|");
            System.out.println("| List Selection:   |");
            System.out.println("| Enter 1 for List  |");
            System.out.println("| Enter 2 for Queue |");
            System.out.println("| Enter 3 for Set   |");
            System.out.println("| Enter 4 for Tree  |");
            System.out.println("| Enter 5 for Comp  |");
            System.out.println("| Enter 6 to Exit   |");
            System.out.println("|...................|");
            accessVar= selector.nextLine();  // Accept Number from User

            //Check if it is a number with try catch
            try {
                n = Integer.parseInt(accessVar);
            } catch (NumberFormatException nfe) {
                System.out.println("Not a number");
                main(new String[]{""});
            }
            numCheck(n);


        System.out.println("Hello World");
    }




}
