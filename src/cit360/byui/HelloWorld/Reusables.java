package cit360.byui.HelloWorld;

import java.util.*;

import static cit360.byui.HelloWorld.HelloWorld.main;

public class Reusables{

    public static void numCheck(int x){

        //Use if statement to check for right number to call correct function
        if(x==1){
            //Call function JList
            jList();
        }else if(x==2){
            //Call function JQueue
            jQUeue();
        }else if(x==3){
            //Call function JSet
            jSet();
        }else if(x==4) {
            //Call function JTree
            jTree();
        }else if(x==5){
                //Call function JTree
                comparable();
        }else if(x==6){
            //Call function JTree
            System.exit(0);
        }else{
            //restart the program
            main(new String[]{""});
        }

    }

    public static void jList(){
        System.out.println("JLIST function called");
        System.out.println("\nAdding my name in 1 and 3 with my kids name in 2 and 4:");
        List<String> jlist=new ArrayList<String>();//Creating arraylist

        jlist.add("Trevon");//Add my own name
        jlist.add("Charvon");//Add my son's name
        jlist.add("Trevon");//Add my own name
        jlist.add("Trevaye");//Add my daughter's name

        //Iterate with advanced For loop
        for (String s : jlist) System.out.println(s);
        //restart the program
        main(new String[]{""});
    }

    public static void jQUeue(){
        System.out.println("JQueue function called");

        PriorityQueue<String> jqueue=new PriorityQueue<String>();

        jqueue.add("Charmayne Morris");//Add my wife's name
        jqueue.add("Trevaye Rianna Morris");//Add my daughter's name
        jqueue.add("Trevon");//Add my own name
        jqueue.add("Charvon");//Add my son's name

        System.out.println("head (element):"+jqueue.element());
        System.out.println("head (peek):"+jqueue.peek());
        System.out.println("iterating the queue elements:");
        //Iterate with advanced For loop
        for (String s : jqueue) System.out.println(s);
        //Remove 2 elements
        jqueue.remove();
        jqueue.poll();
        System.out.println("Use remove and poll func to remove 2 elements:");
        //Iterate with advanced For loop
        for (String s : jqueue) System.out.println(s);
        //restart the program
        main(new String[]{""});
    }

    public static void jSet(){
        System.out.println("JSet function called");

        //Creating HashSet and adding elements
        System.out.println("\nAdding my name in 1, 3 and 4 with my wife in 2:");
        HashSet<String> jset=new HashSet<String>();

        jset.add("Trevon");//Add my own name
        jset.add("Charmayne");//Add my wife's name
        jset.add("Trevon");//Add my own name
        jset.add("Trevon");//Add my own name

        //Iterate with advanced For loop
        for (String s : jset) System.out.println(s);
        //restart the program
        main(new String[]{""});
    }

    public static void jTree(){
        System.out.println("JTree function called");

        //Creating and adding elements
        System.out.println("\nAdding my name in 1 and 3 with my wife and son in 2 and 4:");
        TreeSet<String> jtree=new TreeSet<String>();

        jtree.add("Trevon");//Add my own name
        jtree.add("Charmayne");//Add my wife's name
        jtree.add("Trevon");//Add my own name
        jtree.add("Charvon");//Add my son's name

        //Iterate with advanced For loop
        for (String s : jtree) System.out.println(s);
        //restart the program
        main(new String[]{""});
    }

    public static void comparable(){

        //Create ArrayList to be sorted
        ArrayList<Family> mf=new ArrayList<Family>();

        //Add family order of birth name and age
        mf.add(new Family(1,"Charmayne",42));
        mf.add(new Family(2,"Trevon",39));
        mf.add(new Family(3,"Trevaye",20));
        mf.add(new Family(4,"Charvon",14));

        //Compare the Family name and check from Null to Z
        Comparator<Family> namecm=Comparator.comparing(Family::getName,Comparator.nullsFirst(String::compareTo));
        //Sort the List by the comparator
        Collections.sort(mf,namecm);
        System.out.println("Sort by the Name null to Z");
        for(Family fl: mf) System.out.println(fl.order + " " + fl.name + " " + fl.age);
        //Compare the Family Order and use natural order
        Comparator<Family> ordercm=Comparator.comparing(Family::getOrderno,Comparator.naturalOrder());
        //Sort the List by the comparator
        Collections.sort(mf,ordercm);
        System.out.println("Sort by the Order Number NaturalOrder");
        for(Family fl: mf) System.out.println(fl.order + " " + fl.name + " " + fl.age);
        //Compare the Family Age and use natural order
        Comparator<Family> agecm=Comparator.comparing(Family::getAge,Comparator.naturalOrder());
        //Sort the List by the comparator
        Collections.sort(mf,agecm);
        System.out.println("Sort by the Age least to max using NaturalOrder");
        for(Family fl: mf) System.out.println(fl.order + " " + fl.name + " " + fl.age);
        //Compare the Family Order and use Reverse order
        Comparator<Family> agecmd=Comparator.comparing(Family::getAge,Comparator.reverseOrder());
        //Sort the List by the comparator
        Collections.sort(mf,agecmd);
        System.out.println("Sort by the Age max to least using ReverseOrder");
        for(Family fl: mf) System.out.println(fl.order + " " + fl.name + " " + fl.age);
        main(new String[]{""});
    }


}
