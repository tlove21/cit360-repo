package cit360.byui.HelloWorld;

public class Family {
    int order;
    String name;
    int age;
    public Family(int order, String name, int age){
        this.order=order;
        this.name=name;
        this.age=age;
    }

    public Family() {

    }

    public int getOrderno() {
        return order;
    }
    public void setOrderno(int order) {
        this.order = order;
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }
    public void setAge(int age) {
        this.age = age;
    }
}
